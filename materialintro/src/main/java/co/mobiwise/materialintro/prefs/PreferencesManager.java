package co.mobiwise.materialintro.prefs;

import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;

public class PreferencesManager {

    private static final String PREFERENCES_NAME = "material_intro_preferences";

    private Preferences sharedPreferences;

    public PreferencesManager(Context context) {
        if (context != null) {
            DatabaseHelper databaseHelper = new DatabaseHelper(context.getAbilityPackageContext());
            sharedPreferences = databaseHelper.getPreferences(PREFERENCES_NAME);
        }
    }

    public boolean isDisplayed(String id) {
        return sharedPreferences.getBoolean(id, false);
    }


    public void setDisplayed(String id) {
        sharedPreferences.putBoolean(id, true);
    }

    public void reset(String id) {
        sharedPreferences.putBoolean(id, false);
    }

    public void resetAll() {
        sharedPreferences.clear();
    }
}
