package co.mobiwise.materialintro.target;

import co.mobiwise.materialintro.utils.Utils;
import ohos.agp.components.Component;
import ohos.agp.utils.Point;
import ohos.agp.utils.Rect;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;

import java.util.Arrays;

public class ViewTarget implements Target{

    private Component view;

    public ViewTarget(Component view) {
        this.view = view;
    }

    @Override
    public Point getPoint() {
        int[] location;
        location = view.getLocationOnScreen();
        int statusTab = Utils.getStatusTabHeight(getView().getContext());
        return new Point(location[0] + (view.getWidth() / 2), location[1] + (view.getHeight() / 2)-statusTab);
    }

    @Override
    public Rect getRect() {
        int[] location;
        location = view.getLocationOnScreen();
        int statusTab = Utils.getStatusTabHeight(getView().getContext());
        return new Rect(
                location[0],
                location[1] - statusTab,
                location[0] + view.getWidth(),
                location[1] + view.getHeight() - statusTab
        );
    }

    @Override
    public Component getView() {
        return view;
    }

}
