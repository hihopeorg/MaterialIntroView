package co.mobiwise.materialintro.target;

import ohos.agp.components.Component;
import ohos.agp.utils.Point;
import ohos.agp.utils.Rect;

public interface Target {
    /**
     * Returns center point of target.
     * We can get x and y coordinates using
     * point object
     * @return
     */
    Point getPoint();
    /**
     * Returns Rectangle points of target view
     * @return
     */
    Rect getRect();

    /**
     * return target view
     * @return
     */
    Component getView();
}
