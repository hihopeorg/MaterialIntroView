package co.mobiwise.materialintro.shape;

public enum FocusGravity {

    /**
     * Gravity for focus circle
     * ie. We may want to focus on
     * left of recyclerview item
     */
    LEFT, CENTER, RIGHT
}
