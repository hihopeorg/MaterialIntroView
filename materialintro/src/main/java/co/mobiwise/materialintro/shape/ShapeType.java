package co.mobiwise.materialintro.shape;

public enum ShapeType {
    /**
     * Allows the target area to be highlighted by either a circle or rectangle
     */
    CIRCLE, RECTANGLE
}
