package co.mobiwise.materialintro.shape;

import co.mobiwise.materialintro.target.Target;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;

public class Rect extends Shape {

    RectFloat adjustedRect;

    public Rect(Target target) {
        super(target);
        calculateAdjustedRect();
    }

    public Rect(Target target, Focus focus) {
        super(target, focus);
        calculateAdjustedRect();
    }

    public Rect(Target target, Focus focus, FocusGravity focusGravity, int padding) {
        super(target, focus, focusGravity, padding);
        calculateAdjustedRect();
    }

    @Override
    public void draw(Canvas canvas, Paint eraser, int padding) {
        canvas.drawRoundRect(adjustedRect, padding, padding, eraser);
    }

    private void calculateAdjustedRect() {
        RectFloat rect = new RectFloat(target.getRect());

        rect.left -= padding;
        rect.top -= padding;
        rect.right += padding;
        rect.bottom += padding;

        adjustedRect = rect;
    }

    @Override
    public void reCalculateAll(){
        calculateAdjustedRect();
    }

    @Override
    public Point getPoint(){
        return target.getPoint();
    }

    @Override
    public float getHeight() {
        return (float) adjustedRect.getHeight();
    }

    @Override
    public boolean isTouchOnFocus(double x, double y) {
        return adjustedRect.isInclude((float) x, (float) y);
    }

}
