package co.mobiwise.materialintro.shape;

import co.mobiwise.materialintro.target.Target;
import co.mobiwise.materialintro.utils.Constants;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Point;

public abstract class Shape {

    protected Target target;

    protected Focus focus;

    protected FocusGravity focusGravity;

    protected int padding;

    public Shape(Target target) {
        this(target, Focus.MINIMUM);
    }

    public Shape(Target target, Focus focus) {
        this(target, focus, FocusGravity.CENTER, Constants.DEFAULT_TARGET_PADDING);
    }

    public Shape(Target target, Focus focus, FocusGravity focusGravity, int padding) {
        this.target = target;
        this.focus = focus;
        this.focusGravity = focusGravity;
        this.padding = padding;
    }

    public abstract void draw(Canvas canvas, Paint eraser, int padding);

    protected Point getFocusPoint() {
        if (focusGravity == FocusGravity.LEFT) {

            float xLeft = target.getRect().left + (target.getPoint().getPointX() - target.getRect().left) / 2;
            return new Point(xLeft, target.getPoint().getPointY());
        } else if (focusGravity == FocusGravity.RIGHT) {
            float xRight = target.getPoint().getPointX() + (target.getRect().right - target.getPoint().getPointX()) / 2;
            return new Point(xRight, target.getPoint().getPointY());
        } else
            return target.getPoint();
    }

    public abstract void reCalculateAll();

    public abstract Point getPoint();

    public abstract float getHeight();

    /**
     * Determines if a click is on the shape
     *
     * @param x x-axis location of click
     * @param y y-axis location of click
     * @return true if click is inside shape
     */
    public abstract boolean isTouchOnFocus(double x, double y);
}
