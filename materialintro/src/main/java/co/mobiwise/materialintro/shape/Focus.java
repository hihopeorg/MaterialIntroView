package co.mobiwise.materialintro.shape;

public enum Focus {
    /**
     * Focus on target with circle
     * These enum decides circle radius.
     */
    MINIMUM, NORMAL, ALL
}
