package co.mobiwise.materialintro.animation;


import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;

public class AnimationFactory {
    /**
     * MaterialIntroView will appear on screen with
     * fade in animation. Notifies onAnimationStartListener
     * when fade in animation is about to start.
     */

    public static void animateFadeIn(Component component, long duration, final AnimationListener.OnAnimationStartListener onAnimationStartListener) {

        AnimatorProperty animatorProperty = new AnimatorProperty(component);
        animatorProperty.alphaFrom(0).alpha(1).setDuration(duration);
        animatorProperty.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                if (onAnimationStartListener != null)
                    onAnimationStartListener.onAnimationStart();
            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {

            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        animatorProperty.start();
    }

    /**
     * MaterialIntroView will disappear from screen with
     * fade out animation. Notifies onAnimationEndListener
     * when fade out animation is ended.
     */

    public static void animateFadeOut(Component component, long duration, final AnimationListener.OnAnimationEndListener onAnimationEndListener) {

        AnimatorProperty animatorProperty = new AnimatorProperty(component);
        animatorProperty.alphaFrom(1).alpha(0).setDuration(duration);
        animatorProperty.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                if (onAnimationEndListener != null)
                    onAnimationEndListener.onAnimationEnd();
            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {

            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        animatorProperty.start();
    }

    public static void performAnimation(Component component) {

        createAnimator animatorX = new createAnimator(component);
        createAnimator animatorY = new createAnimator(component);
        /**
         * 设置component 监听，更新UI，必须设置
         */
        component.setBindStateChangedListener(new Component.BindStateChangedListener() {
            @Override
            public void onComponentBoundToWindow(Component component) {
                animatorX.scaleX();
                animatorY.scaleY();
            }

            @Override
            public void onComponentUnboundFromWindow(Component component) {
                animatorX.stop();
                animatorY.stop();
            }
        });
    }

    private static class createAnimator {

        private final AnimatorProperty animatorProperty;
        private final float width;
        private final float height;

        public createAnimator(Component component) {
            /**
             * 获取当前component宽和高
             */
            width = component.getWidth();
            height = component.getHeight();

            /**
             * 创建属性动画实例
             */
            animatorProperty = new AnimatorProperty(component);
            /**
             * 设置透明度
             */
//            animatorProperty.alphaFrom(component.getPivotX()).alpha(0.9f);
            /**
             * 设置循环播放次数
             */
            animatorProperty.setLoopedCount(Animator.INFINITE);
            /**
             * 设置动画变化类型：匀速变化 Animator.CurveType.LINEAR 4
             */
            animatorProperty.setCurveType(Animator.CurveType.SMOOTH_STEP);
            /**
             * 设置持续时间
             */
            animatorProperty.setDuration(1000);
        }

        public void scaleX() {
            animatorProperty.scaleX(0.6f);
            animatorProperty.start();
        }

        public void scaleY() {
            animatorProperty.scaleY(0.6f);
            animatorProperty.start();
        }

        public void stop() {
            animatorProperty.stop();
        }
    }
}
