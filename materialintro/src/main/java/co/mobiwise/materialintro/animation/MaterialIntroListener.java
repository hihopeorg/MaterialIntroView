package co.mobiwise.materialintro.animation;

public interface MaterialIntroListener {

    void onUserClicked(String materialIntroViewId);
}
