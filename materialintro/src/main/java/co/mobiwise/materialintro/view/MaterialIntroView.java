package co.mobiwise.materialintro.view;

import co.mobiwise.materialintro.MaterialIntroConfiguration;
import co.mobiwise.materialintro.ResourceTable;
import co.mobiwise.materialintro.animation.AnimationFactory;
import co.mobiwise.materialintro.animation.AnimationListener;
import co.mobiwise.materialintro.animation.MaterialIntroListener;
import co.mobiwise.materialintro.prefs.PreferencesManager;
import co.mobiwise.materialintro.shape.Circle;
import co.mobiwise.materialintro.shape.Focus;
import co.mobiwise.materialintro.shape.FocusGravity;
import co.mobiwise.materialintro.shape.Rect;
import co.mobiwise.materialintro.shape.Shape;
import co.mobiwise.materialintro.shape.ShapeType;
import co.mobiwise.materialintro.target.Target;
import co.mobiwise.materialintro.target.ViewTarget;
import co.mobiwise.materialintro.utils.Constants;
import co.mobiwise.materialintro.utils.Utils;
import ohos.aafwk.ability.Ability;
import ohos.agp.colors.RgbPalette;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.RectFloat;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.service.Window;
import ohos.agp.window.service.WindowManager;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

/**
 * Created by mertsimsek on 22/01/16.
 */
public class MaterialIntroView extends DependentLayout implements Component.EstimateSizeListener,
        Component.DrawTask, Component.TouchEventListener {

    /**
     * Mask color
     */
    private int maskColor;

    /**
     * MaterialIntroView will start
     * showing after delayMillis seconds
     * passed
     */
    private long delayMillis;

    /**
     * We don't draw MaterialIntroView
     * until isReady field set to true
     */
    private boolean isReady;

    /**
     * Show/Dismiss MaterialIntroView
     * with fade in/out animation if
     * this is enabled.
     */
    private boolean isFadeAnimationEnabled;

    /**
     * Animation duration
     */
    private long fadeAnimationDuration;

    /**
     * targetShape focus on target
     * and clear circle to focus
     */
    private Shape targetShape;

    /**
     * Focus Type
     */
    private Focus focusType;

    /**
     * FocusGravity type
     */
    private FocusGravity focusGravity;

    /**
     * Target View
     */
    private Target targetView;

    /**
     * Eraser
     */
    private Paint eraser;

    /**
     * Handler will be used to
     * delay MaterialIntroView
     */
    private EventHandler handler;

    /**
     * All views will be drawn to
     * this bitmap and canvas then
     * bitmap will be drawn to canvas
     */
    private PixelMap bitmap;
    private Canvas canvas;

    /**
     * Circle padding
     */
    private int padding;

    /**
     * Layout width/height
     */
    private int width = 0;
    private int height = 0;

    /**
     * Dismiss on touch any position
     */
    private boolean dismissOnTouch;

    /**
     * Info dialog view
     */
    private Component infoView;

    /**
     * Info Dialog Text
     */
    private Text textViewInfo;

    /**
     * Info dialog text color
     */
    private int colorTextViewInfo;

    /**
     * Info dialog will be shown
     * If this value true
     */
    private boolean isInfoEnabled;

    /**
     * Dot view will appear center of
     * cleared target area
     */
    private Component dotView;

    /**
     * Dot View will be shown if
     * this is true
     */
    private boolean isDotViewEnabled;

    /**
     * Info Dialog Icon
     */
    private Image imageViewIcon;

    /**
     * Image View will be shown if
     * this is true
     */
    private boolean isImageViewEnabled;

    /**
     * Save/Retrieve status of MaterialIntroView
     * If Intro is already learnt then don't show
     * it again.
     */
    private PreferencesManager preferencesManager;

    /**
     * Check using this Id whether user learned
     * or not.
     */
    private String materialIntroViewId;

    /**
     * When layout completed, we set this true
     * Otherwise onGlobalLayoutListener stuck on loop.
     */
    private boolean isLayoutCompleted;

    /**
     * Notify user when MaterialIntroView is dismissed
     */
    private MaterialIntroListener materialIntroListener;

    /**
     * Perform click operation to target
     * if this is true
     */
    private boolean isPerformClick;

    /**
     * Disallow this MaterialIntroView from showing up more than once at a time
     */
    private boolean isIdempotent;

    /**
     * Shape of target
     */
    private ShapeType shapeType;

    /**
     * Use custom shape
     */
    private boolean usesCustomShape = false;

    private Paint paint;
    private ohos.agp.utils.Rect cardRect = null;
    private Window window;
    private WindowManager windowManager;

    public MaterialIntroView(Context context) {
        super(context);
        init(context);
    }

    public MaterialIntroView(Context context, AttrSet attrs) {
        super(context, attrs);
        init(context);
    }

    public MaterialIntroView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        setVisibility(INVISIBLE);

        /**
         * set default values
         */
        maskColor = Constants.DEFAULT_MASK_COLOR;
        delayMillis = Constants.DEFAULT_DELAY_MILLIS;
        fadeAnimationDuration = Constants.DEFAULT_FADE_DURATION;
        padding = Constants.DEFAULT_TARGET_PADDING;
        colorTextViewInfo = Constants.DEFAULT_COLOR_TEXTVIEW_INFO;
        focusType = Focus.ALL;
        focusGravity = FocusGravity.CENTER;
        shapeType = ShapeType.CIRCLE;
        isReady = false;
        isFadeAnimationEnabled = true;
        dismissOnTouch = false;
        isLayoutCompleted = false;
        isInfoEnabled = false;
        isDotViewEnabled = false;
        isPerformClick = false;
        isImageViewEnabled = true;
        isIdempotent = false;

        /**
         * initialize objects
         */
        handler = new EventHandler(EventRunner.getMainEventRunner());

        preferencesManager = new PreferencesManager(context);

        eraser = new Paint();
        eraser.setBlendMode(BlendMode.CLEAR);
        eraser.setAntiAlias(true);

        paint = new Paint();
        paint.setFilterBitmap(true);
        Component layoutInfo = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_material_intro_card, null, false);

        infoView = layoutInfo.findComponentById(ResourceTable.Id_info_layout);
        textViewInfo = (Text) layoutInfo.findComponentById(ResourceTable.Id_textview_info);
        textViewInfo.setTextColor(new Color(colorTextViewInfo));
        imageViewIcon = (Image) layoutInfo.findComponentById(ResourceTable.Id_imageview_icon);

        dotView = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_dotview, null, false);
        dotView.estimateSize(EstimateSpec.UNCONSTRAINT, EstimateSpec.UNCONSTRAINT);
        getComponentTreeObserver().addTreeLayoutChangedListener(new ComponentTreeObserver.GlobalLayoutListener() {
            @Override
            public void onGlobalLayoutUpdated() {
                targetShape.reCalculateAll();
                if (targetShape != null && targetShape.getPoint().getPointY() != 0 && !isLayoutCompleted) {
                    if (isInfoEnabled)
                        setInfoLayout();
                    if (isDotViewEnabled)
                        setDotViewLayout();
                    removeOnGlobalLayoutListener(MaterialIntroView.this, this);
                }
            }
        });
        setEstimateSizeListener(this);
        addDrawTask(this);
        setTouchEventListener(this);
    }

    public static void removeOnGlobalLayoutListener(Component v, ComponentTreeObserver.GlobalLayoutListener listener) {
        v.getComponentTreeObserver().removeTreeLayoutChangedListener(listener);
    }

    @Override
    public boolean onEstimateSize(int withSpec, int heightSpec) {
        width = EstimateSpec.getSize(withSpec);
        height = EstimateSpec.getSize(heightSpec);
        return false;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (!isReady) return;
        if (bitmap == null || canvas == null) {
            if (bitmap != null) bitmap.release();
            PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
            initializationOptions.size = new Size(width, height);
            initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
            bitmap = PixelMap.create(initializationOptions);
            this.canvas = new Canvas(new Texture(bitmap));
        }

        /**
         * Draw mask
         */
        this.canvas.drawColor(Color.TRANSPARENT.getValue(), BlendMode.CLEAR);
        this.canvas.drawColor(maskColor, BlendMode.SRC);

        /**
         * Clear focus area
         */
        targetShape.draw(this.canvas, eraser, padding);
        canvas.drawPixelMapHolder(new PixelMapHolder(bitmap), 0, 0, paint);
        if (cardRect != null) {
            this.canvas.drawRoundRect(new RectFloat(cardRect), 10, 10, eraser);
        }
    }

    /**
     * Perform click operation when user
     * touches on target circle.
     *
     * @param event
     * @return
     */
    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        MmiPoint point = event.getPointerPosition(event.getIndex());
        float xT = point.getX();
        float yT = point.getY();

        boolean isTouchOnFocus = targetShape.isTouchOnFocus(xT, yT);

        switch (event.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:

                if (isTouchOnFocus && isPerformClick) {
                    targetView.getView().setPressState(true);
                    targetView.getView().invalidate();
                }

                return true;
            case TouchEvent.PRIMARY_POINT_UP:

                if (isTouchOnFocus || dismissOnTouch) {
                    dismiss();
                }


                if (isTouchOnFocus && isPerformClick) {
                    targetView.getView().simulateClick();
                    targetView.getView().setPressState(true);
                    targetView.getView().invalidate();
                    targetView.getView().setPressState(false);
                    targetView.getView().invalidate();
                }

                return true;
            default:
                break;
        }

        return false;
    }

    /**
     * Shows material view with fade in
     * animation
     *
     * @param activity
     */
    private void show(Ability activity) {

        if (preferencesManager.isDisplayed(materialIntroViewId))
            return;
        WindowManager.LayoutConfig mParams = new WindowManager.LayoutConfig();
        mParams.width = ComponentContainer.LayoutConfig.MATCH_PARENT;
        mParams.height = ComponentContainer.LayoutConfig.MATCH_PARENT;
        mParams.flags = WindowManager.LayoutConfig.MARK_TOUCH_MODAL_IMPOSSIBLE |
                WindowManager.LayoutConfig.MARK_FOCUSABLE_IMPOSSIBLE |
                WindowManager.LayoutConfig.MARK_ALLOW_LAYOUT_COVER_SCREEN | WindowManager.LayoutConfig.MARK_WATCH_OUTSIDE_TOUCH;
        int overlay = WindowManager.LayoutConfig.MOD_APPLICATION;
        mParams.type = overlay;
        mParams.pixelFormat = PixelFormat.ARGB_8888.getValue();
        windowManager = WindowManager.getInstance();
        window = windowManager.addComponent(this, getContext(), WindowManager.LayoutConfig.MOD_APPLICATION);
        window.setTransparent(true);
        window.setLayoutConfig(mParams);
        setReady(true);

        handler.postTask(new Runnable() {
            @Override
            public void run() {
                if (isFadeAnimationEnabled)
                    AnimationFactory.animateFadeIn(MaterialIntroView.this, fadeAnimationDuration, new AnimationListener.OnAnimationStartListener() {
                        @Override
                        public void onAnimationStart() {
                            setVisibility(VISIBLE);
                        }
                    });
                else
                    setVisibility(VISIBLE);

            }
        }, delayMillis);

        if (isIdempotent) {
            preferencesManager.setDisplayed(materialIntroViewId);
        }
        targetShape.reCalculateAll();
        if (targetShape != null && targetShape.getPoint().getPointY() != 0 && !isLayoutCompleted) {
            if (isInfoEnabled)
                setInfoLayout();
            if (isDotViewEnabled)
                setDotViewLayout();
        }
    }

    /**
     * Dismiss Material Intro View
     */
    public void dismiss() {
        if (!isIdempotent) {
            preferencesManager.setDisplayed(materialIntroViewId);
        }
        AnimationFactory.animateFadeOut(this, fadeAnimationDuration, new AnimationListener.OnAnimationEndListener() {
            @Override
            public void onAnimationEnd() {
                if (window != null) {
                    windowManager.destroyWindow(window);
                    window = null;
                }
                if (materialIntroListener != null)
                    materialIntroListener.onUserClicked(materialIntroViewId);

            }
        });
    }

    private void removeMaterialView() {
        if (getComponentParent() != null)
            ((ComponentContainer) getComponentParent()).removeComponent(this);
    }

    /**
     * locate info card view above/below the
     * circle. If circle's Y coordiante is bigger than
     * Y coordinate of root view, then locate cardview
     * above the circle. Otherwise locate below.
     */
    private void setInfoLayout() {

        handler.postTask(new Runnable() {
            @Override
            public void run() {
                isLayoutCompleted = true;

                if (infoView.getComponentParent() != null)
                    (infoView.getComponentParent()).removeComponent(infoView);

                DirectionalLayout.LayoutConfig infoDialogParams = new DirectionalLayout.LayoutConfig(
                        ComponentContainer.LayoutConfig.MATCH_PARENT,
                        ComponentContainer.LayoutConfig.MATCH_PARENT);
                if (targetShape.getPoint().getPointY() < height / 2) {
                    ((DirectionalLayout) infoView).setAlignment(LayoutAlignment.TOP);
                    infoDialogParams.setMargins(
                            0,
                            (int) (targetShape.getPoint().getPointY() + targetShape.getHeight() / 2),
                            0,
                            0);
                } else {
                    ((DirectionalLayout) infoView).setAlignment(LayoutAlignment.BOTTOM);
                    infoDialogParams.setMargins(
                            0,
                            0,
                            0,
                            (int) (height - (targetShape.getPoint().getPointY() + targetShape.getHeight() / 2) + 2 * targetShape.getHeight() / 2));
                }

                infoView.setLayoutConfig(infoDialogParams);
                infoView.invalidate();
                addComponent(infoView);
                if (!isImageViewEnabled) {
                    imageViewIcon.setVisibility(HIDE);
                }
                infoView.setVisibility(VISIBLE);
                DirectionalLayout cardView = (DirectionalLayout) infoView.findComponentById(ResourceTable.Id_layout_info_main);
                handler.postTask(() -> {
                    int[] location = cardView.getLocationOnScreen();
                    cardRect = new ohos.agp.utils.Rect(
                            location[0],
                            location[1],
                            location[0] + cardView.getWidth(),
                            location[1] + cardView.getHeight());
                    invalidate();
                }, 100);

            }
        }, 100);
    }

    private void setDotViewLayout() {

        handler.postTask(new Runnable() {
            @Override
            public void run() {
                if (dotView.getComponentParent() != null)
                    ((ComponentContainer) dotView.getComponentParent()).removeComponent(dotView);

                DependentLayout.LayoutConfig dotViewLayoutParams = new DependentLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
                dotViewLayoutParams.height = AttrHelper.vp2px(Constants.DEFAULT_DOT_SIZE, getContext());
                dotViewLayoutParams.width = AttrHelper.vp2px(Constants.DEFAULT_DOT_SIZE, getContext());
                dotViewLayoutParams.setMargins(
                        targetShape.getPoint().getPointXToInt() - (dotViewLayoutParams.width / 2),
                        targetShape.getPoint().getPointYToInt() - (dotViewLayoutParams.height / 2),
                        0,
                        0);
                dotView.setLayoutConfig(dotViewLayoutParams);
                dotView.invalidate();
                addComponent(dotView);

                dotView.setVisibility(VISIBLE);
                AnimationFactory.performAnimation(dotView);
            }
        });
    }

    /**
     * SETTERS
     */

    private void setMaskColor(int maskColor) {
        this.maskColor = maskColor;
    }

    private void setDelay(int delayMillis) {
        this.delayMillis = delayMillis;
    }

    private void enableFadeAnimation(boolean isFadeAnimationEnabled) {
        this.isFadeAnimationEnabled = isFadeAnimationEnabled;
    }

    private void setShapeType(ShapeType shape) {
        this.shapeType = shape;
    }

    private void setReady(boolean isReady) {
        this.isReady = isReady;
    }

    private void setTarget(Target target) {
        targetView = target;
    }

    private void setFocusType(Focus focusType) {
        this.focusType = focusType;
    }

    private void setShape(Shape shape) {
        this.targetShape = shape;
    }

    private void setPadding(int padding) {
        this.padding = padding;
    }

    private void setDismissOnTouch(boolean dismissOnTouch) {
        this.dismissOnTouch = dismissOnTouch;
    }

    private void setFocusGravity(FocusGravity focusGravity) {
        this.focusGravity = focusGravity;
    }

    private void setColorTextViewInfo(int colorTextViewInfo) {
        this.colorTextViewInfo = colorTextViewInfo;
        textViewInfo.setTextColor(new Color(this.colorTextViewInfo));
    }

    private void setTextViewInfo(CharSequence textViewInfo) {
        this.textViewInfo.setText(textViewInfo.toString());
    }

    private void setTextViewInfoSize(int textViewInfoSize) {
        this.textViewInfo.setTextSize(textViewInfoSize);
    }

    private void enableInfoDialog(boolean isInfoEnabled) {
        this.isInfoEnabled = isInfoEnabled;
    }

    private void enableImageViewIcon(boolean isImageViewEnabled) {
        this.isImageViewEnabled = isImageViewEnabled;
    }

    private void setIdempotent(boolean idempotent) {
        this.isIdempotent = idempotent;
    }

    private void enableDotView(boolean isDotViewEnabled) {
        this.isDotViewEnabled = isDotViewEnabled;
    }

    public void setConfiguration(MaterialIntroConfiguration configuration) {

        if (configuration != null) {
            this.maskColor = configuration.getMaskColor();
            this.delayMillis = configuration.getDelayMillis();
            this.isFadeAnimationEnabled = configuration.isFadeAnimationEnabled();
            this.colorTextViewInfo = configuration.getColorTextViewInfo();
            this.isDotViewEnabled = configuration.isDotViewEnabled();
            this.dismissOnTouch = configuration.isDismissOnTouch();
            this.colorTextViewInfo = configuration.getColorTextViewInfo();
            this.focusType = configuration.getFocusType();
            this.focusGravity = configuration.getFocusGravity();
        }
    }

    private void setUsageId(String materialIntroViewId) {
        this.materialIntroViewId = materialIntroViewId;
    }

    private void setListener(MaterialIntroListener materialIntroListener) {
        this.materialIntroListener = materialIntroListener;
    }

    private void setPerformClick(boolean isPerformClick) {
        this.isPerformClick = isPerformClick;
    }


    /**
     * Builder Class
     */
    public static class Builder {

        private MaterialIntroView materialIntroView;

        private Ability activity;

        private Focus focusType = Focus.MINIMUM;

        public Builder(Ability activity) {
            this.activity = activity;
            materialIntroView = new MaterialIntroView(activity);
        }

        public Builder setMaskColor(int maskColor) {
            materialIntroView.setMaskColor(maskColor);
            return this;
        }

        public Builder setDelayMillis(int delayMillis) {
            materialIntroView.setDelay(delayMillis);
            return this;
        }

        public Builder enableFadeAnimation(boolean isFadeAnimationEnabled) {
            materialIntroView.enableFadeAnimation(isFadeAnimationEnabled);
            return this;
        }

        public Builder setShape(ShapeType shape) {
            materialIntroView.setShapeType(shape);
            return this;
        }

        public Builder setFocusType(Focus focusType) {
            materialIntroView.setFocusType(focusType);
            return this;
        }

        public Builder setFocusGravity(FocusGravity focusGravity) {
            materialIntroView.setFocusGravity(focusGravity);
            return this;
        }

        public Builder setTarget(Component view) {
            materialIntroView.setTarget(new ViewTarget(view));
            return this;
        }

        public Builder setTargetPadding(int padding) {
            materialIntroView.setPadding(padding);
            return this;
        }

        public Builder setTextColor(int textColor) {
            materialIntroView.setColorTextViewInfo(textColor);
            return this;
        }

        public Builder setInfoText(CharSequence infoText) {
            materialIntroView.enableInfoDialog(true);
            materialIntroView.setTextViewInfo(infoText);
            return this;
        }

        public Builder setInfoTextSize(int textSize) {
            materialIntroView.setTextViewInfoSize(textSize);
            return this;
        }

        public Builder dismissOnTouch(boolean dismissOnTouch) {
            materialIntroView.setDismissOnTouch(dismissOnTouch);
            return this;
        }

        public Builder setUsageId(String materialIntroViewId) {
            materialIntroView.setUsageId(materialIntroViewId);
            return this;
        }

        public Builder enableDotAnimation(boolean isDotAnimationEnabled) {
            materialIntroView.enableDotView(isDotAnimationEnabled);
            return this;
        }

        public Builder enableIcon(boolean isImageViewIconEnabled) {
            materialIntroView.enableImageViewIcon(isImageViewIconEnabled);
            return this;
        }

        public Builder setIdempotent(boolean idempotent) {
            materialIntroView.setIdempotent(idempotent);
            return this;
        }

        public Builder setConfiguration(MaterialIntroConfiguration configuration) {
            materialIntroView.setConfiguration(configuration);
            return this;
        }

        public Builder setListener(MaterialIntroListener materialIntroListener) {
            materialIntroView.setListener(materialIntroListener);
            return this;
        }

        public Builder setCustomShape(Shape shape) {
            materialIntroView.usesCustomShape = true;
            materialIntroView.setShape(shape);
            return this;
        }

        public Builder performClick(boolean isPerformClick) {
            materialIntroView.setPerformClick(isPerformClick);
            return this;
        }

        public MaterialIntroView build() {
            if (materialIntroView.usesCustomShape) {
                return materialIntroView;
            }

            // no custom shape supplied, build our own
            Shape shape;

            if (materialIntroView.shapeType == ShapeType.CIRCLE) {
                shape = new Circle(
                        materialIntroView.targetView,
                        materialIntroView.focusType,
                        materialIntroView.focusGravity,
                        materialIntroView.padding);
            } else {
                shape = new Rect(
                        materialIntroView.targetView,
                        materialIntroView.focusType,
                        materialIntroView.focusGravity,
                        materialIntroView.padding);
            }

            materialIntroView.setShape(shape);
            return materialIntroView;
        }

        public MaterialIntroView show() {
            build().show(activity);
            return materialIntroView;
        }

    }

}
