/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package co.mobiwise.materialintro.utils;

import ohos.agp.components.AttrHelper;
import ohos.agp.utils.Point;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

public class Utils {
    /**
     * vp 虚拟像素单位：虚拟像素是一种可灵活使用和缩放的单位，它与屏幕像素的关系是 1vp 约等于 160dpi 屏幕密度设备上的 1px
     * fp 字体像素大小默认情况下与vp相同，但当用户在设置中修改了字体显示大小，那么字体大小则会在vp的基础上乘以 scale 系数。即默认情况下 1 fp = 1vp，如果设置了字体显示大小 1fp = 1vp * scale。
     * <p>
     * px：即像素，是手机的物理尺寸，如手机1920*1080就是宽度方向上有1080个像素点，高度方向上有1920个像素点
     * <p>
     * dpi：是一个相对尺寸，即每英寸的像素点数
     * <p>
     * dp：一个相对单位，与手机物理像素点无关。
     * <p>
     * 相对密度:getResources().getDisplayMetrics().density
     */

    public static float vpToDp(float vp, Context context) {
        return (float) (vp / AttrHelper.getDensity(context));
    }

    public static float dpToVp(float dp, Context context) {
        return (float) (dp * AttrHelper.getDensity(context));
    }

    public static int getStatusTabHeight(Context context){
        return 129;
    }
}
