package co.mobiwise.materialintro.utils;

public class Constants {

    public final static int DEFAULT_MASK_COLOR = 0x70000000;

    public final static long DEFAULT_DELAY_MILLIS = 0;

    public final static long DEFAULT_FADE_DURATION = 700;

    public final static int DEFAULT_TARGET_PADDING = 10;

    public final static int DEFAULT_COLOR_TEXTVIEW_INFO = 0xFF000000;

    public final static int DEFAULT_DOT_SIZE = 55;

}
