package co.mobiwise.sample.units;

import co.mobiwise.materialintro.animation.AnimationFactory;
import co.mobiwise.materialintro.utils.Utils;
import co.mobiwise.sample.ShapeFactory;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Button;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ShapeFactoryTest {

    private Context context;

    @Before
    public void setUp() throws Exception {
        IAbilityDelegator abilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        context = abilityDelegator.getAppContext();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void componentCreateCircular() {
        Button button = new Button(context);
        RgbColor rgbColor = new RgbColor(255, 255, 255);
        int width = 100;
        int height = 100;
        Button buttonCircular = (Button) ShapeFactory.createCircular(button, context, rgbColor, width, height);
        Assert.assertNotNull("buttonCircular is null", buttonCircular);
    }

    @Test
    public void createRadius() {
        Button button = new Button(context);
        Button buttonRadius = (Button) ShapeFactory.createRadius(button, context);
        Assert.assertNotNull("buttonRadius is null", buttonRadius);

    }

    @Test
    public void createShapeElement() {
        ShapeElement shapeElement = ShapeFactory.initChildShapeElement(context);
        Assert.assertNotNull("shapeElement is null", shapeElement);
    }

    @Test
    public void createAlpha() {
        Text shape = new Text(context);
        int alpha = 125;//0-255
        ShapeFactory.createAlpha(shape, alpha);
        Assert.assertNotNull("shape is null", shape);
    }

    @Test
    public void performAnimation() {
        Text shape = new Text(context);
        AnimationFactory.performAnimation(shape);
        Assert.assertNotNull("shape is null", shape);
    }

    @Test
    public void dpToVp() {
        float v = Utils.dpToVp(360, context);
        Assert.assertTrue("It's right", 1080.0 == v);
    }

    @Test
    public void vpToDp() {
        float v = Utils.vpToDp(1080, context);
        Assert.assertTrue("It's right", 360.0 == v);
    }

    @Test
    public void isDisplayed() {
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        Preferences sharedPreferences = databaseHelper.getPreferences("PreferencesManagerTest");
        boolean isId = sharedPreferences.getBoolean("id", true);
        Assert.assertEquals("isId is false", true,isId);
    }

    @Test
    public void setDisplayed() {
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        Preferences sharedPreferences = databaseHelper.getPreferences("PreferencesManagerTest");
        Preferences id = sharedPreferences.putBoolean("id", true);
        Assert.assertNotNull("isId is false", id);
    }
}