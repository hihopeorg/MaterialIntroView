package co.mobiwise.sample.UItest;

import co.mobiwise.sample.EventHelper;
import co.mobiwise.sample.MainAbility;
import co.mobiwise.sample.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.ListContainer;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UITest {
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
        Thread.sleep(2000);
    }

    @Test
    public void startMainUi() throws InterruptedException {
        Ability ability = EventHelper.startAbility(MainAbility.class);
        Image btn = (Image) ability.findComponentById(ResourceTable.Id_img_left);

        Thread.sleep(1000);
        EventHelper.triggerClickEvent(ability, btn);
        Thread.sleep(2000);
        Assert.assertNotNull("It's right", btn);
        sAbilityDelegator.stopAbility(ability);
    }

    @Test
    public void startMainFrction() throws InterruptedException {
        MainAbility ability = EventHelper.startAbility(MainAbility.class);
        Image btn = (Image) ability.findComponentById(ResourceTable.Id_img_left);
        EventHelper.triggerClickEvent(ability, btn);
        Thread.sleep(1000);
        Component dailogView = ability.getRootView();

        ListContainer listContainer = (ListContainer) dailogView.findComponentById(ResourceTable.Id_list_container);
        Thread.sleep(1000);
        EventHelper.triggerClickEvent(ability, listContainer.getComponentAt(0));
        Thread.sleep(2000);

        Assert.assertNotNull("It's right", listContainer);
        sAbilityDelegator.stopAbility(ability);

    }

    @Test
    public void startGravityFrction() throws InterruptedException {
        MainAbility ability = EventHelper.startAbility(MainAbility.class);
        Image btn = (Image) ability.findComponentById(ResourceTable.Id_img_left);
        EventHelper.triggerClickEvent(ability, btn);
        Thread.sleep(1000);
        Component dailogView = ability.getRootView();

        ListContainer listContainer = (ListContainer) dailogView.findComponentById(ResourceTable.Id_list_container);
        Thread.sleep(1000);
        EventHelper.triggerClickEvent(ability, listContainer.getComponentAt(1));
        Thread.sleep(2000);

        Assert.assertNotNull("It's right", listContainer);
        sAbilityDelegator.stopAbility(ability);
    }

    @Test
    public void startFocusFraction() throws InterruptedException {
        MainAbility ability = EventHelper.startAbility(MainAbility.class);
        Image btn = (Image) ability.findComponentById(ResourceTable.Id_img_left);
        EventHelper.triggerClickEvent(ability, btn);
        Thread.sleep(1000);
        Component dailogView = ability.getRootView();

        ListContainer listContainer = (ListContainer) dailogView.findComponentById(ResourceTable.Id_list_container);
        Thread.sleep(1000);
        EventHelper.triggerClickEvent(ability, listContainer.getComponentAt(2));
        Thread.sleep(2000);

        sAbilityDelegator.stopAbility(ability);
    }

    @Test
    public void startRecyclerviewFrction() throws InterruptedException {
        MainAbility ability = EventHelper.startAbility(MainAbility.class);
        Image btn = (Image) ability.findComponentById(ResourceTable.Id_img_left);
        EventHelper.triggerClickEvent(ability, btn);
        Thread.sleep(1000);
        Component dailogView = ability.getRootView();

        ListContainer listContainer = (ListContainer) dailogView.findComponentById(ResourceTable.Id_list_container);
        Thread.sleep(1000);
        EventHelper.triggerClickEvent(ability, listContainer.getComponentAt(3));
        Thread.sleep(2000);

        Assert.assertNotNull("It's right", listContainer);
        sAbilityDelegator.stopAbility(ability);
    }

    @Test
    public void startToolbarMenuItem() throws InterruptedException {
        MainAbility ability = EventHelper.startAbility(MainAbility.class);
        Image btn = (Image) ability.findComponentById(ResourceTable.Id_img_left);
        EventHelper.triggerClickEvent(ability, btn);
        Thread.sleep(1000);
        Component dailogView = ability.getRootView();

        ListContainer listContainer = (ListContainer) dailogView.findComponentById(ResourceTable.Id_list_container);
        Thread.sleep(1000);
        EventHelper.triggerClickEvent(ability, listContainer.getComponentAt(4));
        Thread.sleep(2000);

        Assert.assertNotNull("It's right", listContainer);
        sAbilityDelegator.stopAbility(ability);
    }
}