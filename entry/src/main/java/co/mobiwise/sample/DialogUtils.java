/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package co.mobiwise.sample;

import ohos.agp.components.AttrHelper;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;

public class DialogUtils {
    private static Component mComponent;

    public static void createCommonDialog(Component component, Context context, String mTitle) {

        mComponent = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_item_testDialog, null, false);

        CommonDialog commonDialog = new CommonDialog(context);
        commonDialog.setSize(AttrHelper.vp2px(300, context), AttrHelper.vp2px(100, context));
        commonDialog.setAutoClosable(true);
        commonDialog.setContentCustomComponent(mComponent);
        commonDialog.show();

        Text title = (Text) mComponent.findComponentById(ResourceTable.Id_title);
        title.setText(mTitle);

    }
}
