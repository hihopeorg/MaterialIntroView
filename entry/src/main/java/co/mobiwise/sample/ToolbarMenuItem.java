package co.mobiwise.sample;

import co.mobiwise.materialintro.animation.MaterialIntroListener;
import co.mobiwise.materialintro.shape.Focus;
import co.mobiwise.materialintro.shape.FocusGravity;
import co.mobiwise.materialintro.view.MaterialIntroView;
import co.mobiwise.sample.slice.ToolbarMenuItemSlice;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.ToastDialog;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

public class ToolbarMenuItem extends FractionAbility implements MaterialIntroListener {

    private static final String MENU_SHARED_ID_TAG = "menuSharedIdTag";
    private static final String MENU_ABOUT_ID_TAG = "menuAboutIdTag";
    private static final String MENU_SEARCH_ID_TAG = "menuSearchIdTag";
    private Image mIvAbout;
    private Image mIvShare;
    private EventHandler eventHandler;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ToolbarMenuItemSlice.class.getName());
        super.setUIContent(ResourceTable.Layout_ability_main_tal);
        initView();
    }

    private void initView() {
        Image imageLeft = (Image) findComponentById(ResourceTable.Id_img_left);
        Image search = (Image) findComponentById(ResourceTable.Id_img_search);
        mIvAbout = (Image) findComponentById(ResourceTable.Id_img_help);
        mIvShare = (Image) findComponentById(ResourceTable.Id_img_share);
        eventHandler = new EventHandler(EventRunner.getMainEventRunner());
        eventHandler.postTask(()->{
            //show the intro view
            showIntro(search, MENU_SEARCH_ID_TAG, "Welcome. Let\\'s setup your profile. Just tap on the circle.", FocusGravity.CENTER);
        },100);
    }

    /**
     * Method that handles display of intro screens
     *
     * @param view         View to show guide
     * @param id           Unique ID
     * @param text         Display message
     * @param focusGravity Focus Gravity of the display
     */
    public void showIntro(Component view, String id, String text, FocusGravity focusGravity) {
        new MaterialIntroView.Builder(this)
                .enableDotAnimation(true)
                .setFocusGravity(focusGravity)
                .setFocusType(Focus.MINIMUM)
                .setDelayMillis(100)
                .enableFadeAnimation(true)
                .performClick(true)
                .setInfoText(text)
                .setTarget(view)
                .setListener(this)
                .setUsageId(id)
                .show();
    }

    @Override
    public void onUserClicked(String materialIntroViewId) {
        switch (materialIntroViewId) {
            case MENU_SEARCH_ID_TAG:
                showIntro(mIvAbout, MENU_ABOUT_ID_TAG, "Welcome. Let\\'s setup your profile. Just tap on the circle.", FocusGravity.LEFT);
                break;
            case MENU_ABOUT_ID_TAG:
                showIntro(mIvShare, MENU_SHARED_ID_TAG, "Welcome. Let\\'s setup your profile. Just tap on the circle.", FocusGravity.LEFT);
                break;
            case MENU_SHARED_ID_TAG:
                ToastDialog toastDialog = new ToastDialog(getContext());
                toastDialog.setText("Complete!");
                toastDialog.show();
                break;
            default:
                break;
        }
    }
}
