package co.mobiwise.sample;

import co.mobiwise.sample.frction.FocusFraction;
import co.mobiwise.sample.frction.GravityFrction;
import co.mobiwise.sample.frction.MainFrction;
import co.mobiwise.sample.frction.RecyclerviewFrction;
import co.mobiwise.sample.slice.SampleItemProvider;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.PopupDialog;

import java.util.ArrayList;
import java.util.List;

public class MainAbility extends FractionAbility {
    private StackLayout statckLayout;
    private ListContainer listContainer;
    private int statckLayoutId;
    private PopupDialog dialog;
    private static Component view;
    private List<Fraction> fractions = new ArrayList<>();
    private int fractionPosition = 0;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initView();
    }

    @Override
    protected void onActive() {
        super.onActive();
        initFraction();
    }

    private void initView() {
        statckLayout = (StackLayout) findComponentById(ResourceTable.Id_stackLayout_main);
        statckLayoutId = ResourceTable.Id_stackLayout_main;

        Image leftMore = (Image) findComponentById(ResourceTable.Id_img_left);
        leftMore.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                cretaeCommonDialog();
            }
        });
    }

    public Component getRootView() {
        return view;
    }

    private void cretaeCommonDialog() {
        Component listParse = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_listContainer, null, false);
        view = listParse;
        dialog = new PopupDialog(getContext(), listParse);
        dialog.setSize(AttrHelper.fp2px(300, getContext()), statckLayout.getHeight());
        dialog.setAutoClosable(true);
        dialog.setCustomComponent(listParse);
        listContainer = (ListContainer) listParse.findComponentById(ResourceTable.Id_list_container);
        String[] list = new String[]{"Demo Example", "Gravity Example", "Focus Example", "RecyclerView Example", "Toolbar Example", "Tab Example"};
        List<String> listItem = new ArrayList<>();
        for (int i = 0; i < list.length; i++) {
            listItem.add(list[i]);
        }

        dialog.showOnCertainPosition(LayoutAlignment.START, 0, 120);
        SampleItemProvider sampleItemProvider = new SampleItemProvider(listItem, getContext());
        listContainer.setItemProvider(sampleItemProvider);
        /**
         * 适配 测试用例
         */
        sampleItemProvider.setItemListener(new SampleItemProvider.ListenerCallBack() {
            @Override
            public void onItemListener(Component component, int position) {
                onItemClicked(component, position);
            }
        });
    }

    private void initFraction() {
        fractions.add(new MainFrction(getContext()));
        fractions.add(new GravityFrction(getContext()));
        fractions.add(new FocusFraction(getContext()));
        fractions.add(new RecyclerviewFrction(getContext()));
        this.getFractionManager()
                .startFractionScheduler()
                .replace(statckLayoutId, fractions.get(fractionPosition))
                .submit();
    }

    public void onItemClicked(Component component, int position) {
        switch (position) {
            case 0:
            case 1:
            case 2:
            case 3:
                fractionMangerFun(dialog, fractions.get(position));
                fractionPosition = position;
                break;
            case 4:
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("co.mobiwise.sample")
                        .withAbilityName(ToolbarMenuItem.class.getName())
                        .build();
                intent.setOperation(operation);
                startAbility(intent);
//                present(new ToolbarMenuItemSlice(), new Intent());
                dialog.destroy();
                break;
            case 5:
                dialog.destroy();
                break;
        }
    }

    private void fractionMangerFun(PopupDialog dialog, Fraction fraction) {
        this.getFractionManager()
                .startFractionScheduler()
                .replace(statckLayoutId, fraction)
                .submit();
        dialog.destroy();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onBackground() {
        super.onBackground();
    }

    @Override
    protected void onInactive() {
        super.onInactive();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onBackPressed() {
        super.onBackPressed();
    }
}
