package co.mobiwise.sample.frction;

import co.mobiwise.materialintro.animation.AnimationFactory;
import co.mobiwise.materialintro.animation.MaterialIntroListener;
import co.mobiwise.materialintro.prefs.PreferencesManager;
import co.mobiwise.materialintro.shape.Focus;
import co.mobiwise.materialintro.shape.FocusGravity;
import co.mobiwise.materialintro.view.MaterialIntroView;
import co.mobiwise.sample.ResourceTable;
import co.mobiwise.sample.ShapeFactory;
import co.mobiwise.sample.model.Song;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.multimodalinput.event.TouchEvent;

import java.util.ArrayList;
import java.util.List;

public class RecyclerviewFrction extends Fraction implements MaterialIntroListener, Component.TouchEventListener {
    private static final String INTRO_CARD = "recyclerView_material_intro";

    private final Context context;
    private ListContainer listContainer;
    private RecyclerviewBaseItemProvider recyclerviewBaseItemProvider;
    private EventHandler eventHandler;

    public RecyclerviewFrction(Context context) {
        this.context = context;
    }

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_fraction_recyclerview, null, false);

        listContainer = (ListContainer) component.findComponentById(ResourceTable.Id_recycler_view);
        initializeRecyclerview();
        eventHandler = new EventHandler(EventRunner.getMainEventRunner());
        eventHandler.postTask(()->{
            showMaterialIntro();
        },100);
        return component;
    }

    private void showMaterialIntro() {

        new MaterialIntroView.Builder(getFractionAbility())
                .enableDotAnimation(true)
                .setFocusGravity(FocusGravity.CENTER)
                .setFocusType(Focus.MINIMUM)
                .setDelayMillis(200)
                .enableFadeAnimation(true)
                .setListener(this)
                .performClick(true)
                .setInfoText("This intro focuses on Recyclerview item")
                .setTarget(listContainer.getComponentAt(2))
                .setUsageId(INTRO_CARD) //THIS SHOULD BE UNIQUE ID
                .show();


    }

    private void initializeRecyclerview() {
        recyclerviewBaseItemProvider = new RecyclerviewBaseItemProvider(context);
        loadData();
        listContainer.setItemProvider(recyclerviewBaseItemProvider);

        TableLayoutManager tableLayoutManager = new TableLayoutManager();
        tableLayoutManager.setColumnCount(2);

        listContainer.setLayoutManager(tableLayoutManager);
    }

    private void loadData() {
        Song song = new Song("Diamond", ResourceTable.Media_diamond, "Rihanna");
        List<Song> songList = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            songList.add(song);
        }
        recyclerviewBaseItemProvider.setSongList(songList);
    }

    @Override
    public void onUserClicked(String materialIntroViewId) {
        if (materialIntroViewId.equals(INTRO_CARD)) {
            new ToastDialog(getContext()).setText("User Clicked").show();
        }
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {

        return false;
    }
}
