/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package co.mobiwise.sample.frction;

import co.mobiwise.sample.ResourceTable;
import co.mobiwise.sample.model.Song;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

public class RecyclerviewBaseItemProvider extends BaseItemProvider {
    private final Context context;
    private List<Song> songList;

    public RecyclerviewBaseItemProvider(Context context) {
        this.context = context;
        songList = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return songList == null ? 0 : songList.size();
    }

    @Override
    public Object getItem(int position) {
        return songList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        Component parse;
        SettingHolder holder;

        Song song = songList.get(position);
        if (component == null) {
            parse = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_list_item_card, componentContainer, false);
            holder = new SettingHolder(parse);
            parse.setTag(holder);
        } else {
            parse = component;
            holder = (SettingHolder) parse.getTag();
        }
        holder.coverIamge.setPixelMap(song.songArt);
        holder.coverName.setText(song.songName);
        holder.singerName.setText(song.singerName);
        return parse;
    }

    public void setSongList(List<Song> songList) {
        this.songList = songList;
        notifyDataChanged();
    }

    private class SettingHolder {

        private final Image coverIamge;
        private final Text coverName;
        private final Text singerName;

        public SettingHolder(Component parse) {
            coverIamge = (Image) parse.findComponentById(ResourceTable.Id_cover_photo);
            coverName = (Text) parse.findComponentById(ResourceTable.Id_cover_name);
            singerName = (Text) parse.findComponentById(ResourceTable.Id_singer_name);
        }
    }
}
