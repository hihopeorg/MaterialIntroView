package co.mobiwise.sample.frction;

import co.mobiwise.materialintro.animation.AnimationFactory;
import co.mobiwise.materialintro.prefs.PreferencesManager;
import co.mobiwise.materialintro.shape.Focus;
import co.mobiwise.materialintro.shape.FocusGravity;
import co.mobiwise.materialintro.shape.ShapeType;
import co.mobiwise.materialintro.view.MaterialIntroView;
import co.mobiwise.sample.ResourceTable;
import co.mobiwise.sample.ShapeFactory;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;
import ohos.data.preferences.Preferences;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

public class MainFrction extends Fraction implements Component.ClickedListener {
    private static final String INTRO_CARD = "material_intro";

    private final Context context;
    ComponentContainer component;
    private DirectionalLayout directionalLayout;
    private EventHandler eventHandler;

    public MainFrction(Context context) {
        this.context = context;
    }

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        component = (ComponentContainer) LayoutScatter.getInstance(context).parse(ResourceTable.Layout_fraction_main, null, false);
        directionalLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_dir_layout);
        Button resetBtn = (Button) component.findComponentById(ResourceTable.Id_button_reset_all);
        resetBtn.setClickedListener(this);
        resetBtn.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                int id = component.getId();
                if (id == ResourceTable.Id_button_reset_all)
                    new PreferencesManager(context).resetAll();
            }
        });
        return component;
    }

    @Override
    protected void onActive() {
        super.onActive();
        eventHandler = new EventHandler(EventRunner.getMainEventRunner());
        eventHandler.postTask(new Runnable() {
            @Override
            public void run() {
                showIntro(directionalLayout, INTRO_CARD, "This is card! Hello There. You can set this text!");
            }
        },100);
    }

    private void showIntro(Component view, String usageId, String text){
        new MaterialIntroView.Builder(getFractionAbility())
                .enableDotAnimation(true)
                //.enableIcon(false)
                .setFocusGravity(FocusGravity.CENTER)
                .setFocusType(Focus.MINIMUM)
                .setDelayMillis(200)
                .enableFadeAnimation(true)
                .performClick(true)
                .setInfoText(text)
                .setTarget(view)
                .setShape(ShapeType.RECTANGLE)
                .setUsageId(usageId) //THIS SHOULD BE UNIQUE ID
                .show();
    }
    @Override
    public void onClick(Component component) {
        int id = component.getId();

        if(id == ResourceTable.Id_button_reset_all)
            new PreferencesManager(getFractionAbility().getApplicationContext()).resetAll();
    }
}
