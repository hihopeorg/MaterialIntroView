package co.mobiwise.sample.frction;

import co.mobiwise.materialintro.animation.MaterialIntroListener;
import co.mobiwise.materialintro.prefs.PreferencesManager;
import co.mobiwise.materialintro.shape.Focus;
import co.mobiwise.materialintro.shape.FocusGravity;
import co.mobiwise.materialintro.view.MaterialIntroView;
import co.mobiwise.sample.ResourceTable;
import co.mobiwise.sample.ShapeFactory;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

public class FocusFraction extends Fraction implements MaterialIntroListener {
    private final Context context;

    private static final String INTRO_FOCUS_1 = "intro_focus_1";
    private static final String INTRO_FOCUS_2 = "intro_focus_2";
    private static final String INTRO_FOCUS_3 = "intro_focus_3";

    private Button button1;
    private Button button2;
    private Button button3;
    private EventHandler eventHandler;

    public FocusFraction(Context context) {
        this.context = context;
    }

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_fraction_focus, null, false);
        button1 = (Button) component.findComponentById(ResourceTable.Id_button_focus_1);
        button2 = (Button) component.findComponentById(ResourceTable.Id_button_focus_2);
        button3 = (Button) component.findComponentById(ResourceTable.Id_button_focus_3);
        eventHandler = new EventHandler(EventRunner.getMainEventRunner());
        eventHandler.postTask(()->{
            showIntro(button1, INTRO_FOCUS_1, "This intro view focus on all target.", Focus.ALL);
        },100);
        return component;
    }

    public void showIntro(Component component, String id, String text, Focus focusType) {
        new MaterialIntroView.Builder(getFractionAbility())
                .enableDotAnimation(false)
                .setFocusGravity(FocusGravity.CENTER)
                .setFocusType(focusType)
                .setDelayMillis(200)
                .enableFadeAnimation(true)
                .setListener(this)
                .performClick(true)
                .setInfoText(text)
                .setTarget(component)
                .setUsageId(id) //THIS SHOULD BE UNIQUE ID
                .show();
    }

    @Override
    public void onUserClicked(String materialIntroViewId) {
        if (materialIntroViewId.equals(INTRO_FOCUS_1))
            showIntro(button2, INTRO_FOCUS_2, "This intro view focus on minimum size", Focus.MINIMUM);
        else if (materialIntroViewId.equals(INTRO_FOCUS_2))
            showIntro(button3, INTRO_FOCUS_3, "This intro view focus on normal size (avarage of MIN and ALL)", Focus.NORMAL);
    }
}
