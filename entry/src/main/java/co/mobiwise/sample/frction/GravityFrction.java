package co.mobiwise.sample.frction;

import co.mobiwise.materialintro.animation.AnimationFactory;
import co.mobiwise.materialintro.animation.MaterialIntroListener;
import co.mobiwise.materialintro.prefs.PreferencesManager;
import co.mobiwise.materialintro.shape.Focus;
import co.mobiwise.materialintro.shape.FocusGravity;
import co.mobiwise.materialintro.view.MaterialIntroView;
import co.mobiwise.sample.ResourceTable;
import co.mobiwise.sample.ShapeFactory;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

public class GravityFrction extends Fraction implements MaterialIntroListener {
    private final Context context;

    private static final String INTRO_CARD1 = "intro_card_1";
    private static final String INTRO_CARD2 = "intro_card_2";
    private static final String INTRO_CARD3 = "intro_card_3";

    private DirectionalLayout cardView1;
    private DirectionalLayout cardView2;
    private DirectionalLayout cardView3;
    private EventHandler eventHandler;

    public GravityFrction(Context context) {
        this.context = context;
    }

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_fraction_gravity, null, false);
        cardView1 = (DirectionalLayout) component.findComponentById(ResourceTable.Id_dir_layout1);
        cardView2 = (DirectionalLayout) component.findComponentById(ResourceTable.Id_dir_layout2);
        cardView3 = (DirectionalLayout) component.findComponentById(ResourceTable.Id_dir_layout3);
        eventHandler = new EventHandler(EventRunner.getMainEventRunner());
        eventHandler.postTask(()->{
            showIntro(cardView1, INTRO_CARD1, "This intro focuses on RIGHT", FocusGravity.RIGHT);
        },100);

        return component;
    }

    private void showIntro(Component component, String id, String text, FocusGravity focusGravity) {
       new MaterialIntroView.Builder(getFractionAbility())
                .enableDotAnimation(true)
                .setFocusGravity(focusGravity)
                .setFocusType(Focus.MINIMUM)
                .setDelayMillis(200)
                .enableFadeAnimation(true)
                .performClick(true)
                .setInfoText(text)
                .setTarget(component)
                .setListener(this)
                .setUsageId(id) //THIS SHOULD BE UNIQUE ID
                .show();
    }

    @Override
    public void onUserClicked(String materialIntroViewId) {
        if (materialIntroViewId.equals(INTRO_CARD1))
            showIntro(cardView2, INTRO_CARD2, "This intro focuses on CENTER.", FocusGravity.CENTER);
        if (materialIntroViewId.equals(INTRO_CARD2))
            showIntro(cardView3, INTRO_CARD3, "This intro focuses on LEFT.", FocusGravity.LEFT);
    }
}
