package co.mobiwise.sample;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;

import static ohos.agp.components.element.ShapeElement.OVAL;

public class ShapeFactory {

    public static Component createCircular(Component component, Context context, RgbColor rgbColor, int width, int height) {

        component.setWidth(width);
        component.setHeight(height);

        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setShape(OVAL);
        shapeElement.setRgbColor(rgbColor);
        component.setBackground(shapeElement);

        return component;
    }

    /**
     * 设置不同的圆角
     */
    public static Component createRadius(Component component, Context context) {


        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setShape(ShapeElement.RECTANGLE);
        shapeElement.setCornerRadius(10f);
        shapeElement.setCornerRadiiArray(new float[]{20, 40, 30, 60, 20, 20, 100, 120});
        component.setBackground(shapeElement);

        return component;
    }

    /**
     * 设置透明度
     */
    public static void createAlpha(Component component, int alpha) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setShape(OVAL);
        shapeElement.setAlpha(alpha);
        component.setBackground(shapeElement);
    }

    /**
     * 设置圆背景
     */
    public static void createCirCle(Component component, RgbColor rgbColor,int alpha) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setShape(OVAL);
        shapeElement.setRgbColor(rgbColor);
        shapeElement.setAlpha(alpha);
        component.setBackground(shapeElement);
    }


    /**
     * 初始化子组件背景
     *
     * @param context
     * @return
     */
    public static ShapeElement initChildShapeElement(Context context) {
        ShapeElement element = new ShapeElement();
        element.setRgbColor(new RgbColor(34, 197, 255));
        return element;
    }
}
