package co.mobiwise.sample.slice;

import co.mobiwise.sample.ResourceTable;
import co.mobiwise.sample.frction.FocusFraction;
import co.mobiwise.sample.frction.GravityFrction;
import co.mobiwise.sample.frction.MainFrction;
import co.mobiwise.sample.frction.RecyclerviewFrction;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.PopupDialog;

import java.util.ArrayList;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice {

    private StackLayout statckLayout;
    private ListContainer listContainer;
    private int statckLayoutId;
    private PopupDialog dialog;
    private Component view;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initView();
    }

    private void initView() {
        statckLayout = (StackLayout) findComponentById(ResourceTable.Id_stackLayout_main);
        statckLayoutId = ResourceTable.Id_stackLayout_main;

        ((FractionAbility) getAbility()).getFractionManager()
                .startFractionScheduler()
                .replace(statckLayoutId, new MainFrction(getContext()))
                .submit();

        Image leftMore = (Image) findComponentById(ResourceTable.Id_img_left);
        leftMore.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                cretaeCommonDialog();
            }
        });
    }

    public Component getRootView() {
        return view;
    }

    private void cretaeCommonDialog() {
        Component listParse = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_listContainer, null, false);
        view = listParse;
        dialog = new PopupDialog(getContext(), listParse);
        dialog.setSize(AttrHelper.fp2px(300, getContext()), statckLayout.getHeight());
        dialog.setAutoClosable(true);
        dialog.setCustomComponent(listParse);
        listContainer = (ListContainer) listParse.findComponentById(ResourceTable.Id_list_container);
        String[] list = new String[]{"Demo Example", "Gravity Example", "Focus Example", "RecyclerView Example", "Toolbar Example", "Tab Example"};
        List<String> listItem = new ArrayList<>();
        for (int i = 0; i < list.length; i++) {
            listItem.add(list[i]);
        }

        dialog.showOnCertainPosition(LayoutAlignment.START, 0, 120);
        SampleItemProvider sampleItemProvider = new SampleItemProvider(listItem, getContext());
        listContainer.setItemProvider(sampleItemProvider);
        /**
         * 适配 测试用例
         */
        sampleItemProvider.setItemListener(new SampleItemProvider.ListenerCallBack() {
            @Override
            public void onItemListener(Component component, int position) {
                onItemClicked(component, position);
            }
        });
    }

    public void onItemClicked(Component component, int position) {
        switch (position) {
            case 0:
                FractionMangerFun(dialog, new MainFrction(getContext()));
                break;
            case 1:
                FractionMangerFun(dialog, new GravityFrction(getContext()));
                break;
            case 2:
                FractionMangerFun(dialog, new FocusFraction(getContext()));
                break;
            case 3:
                FractionMangerFun(dialog, new RecyclerviewFrction(getContext()));
                break;
            case 4:
                present(new ToolbarMenuItemSlice(), new Intent());
                dialog.destroy();
                break;
            case 5:
                dialog.destroy();
                break;
        }
    }

    private void FractionMangerFun(PopupDialog dialog, Fraction fraction) {
        ((FractionAbility) getAbility()).getFractionManager()
                .startFractionScheduler()
                .replace(statckLayoutId, fraction)
                .submit();
        dialog.destroy();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onBackground() {
        super.onBackground();
    }

    @Override
    protected void onInactive() {
        super.onInactive();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void terminateAbility() {
        super.terminateAbility();
    }

    @Override
    protected void onBackPressed() {
        super.onBackPressed();
    }
}
