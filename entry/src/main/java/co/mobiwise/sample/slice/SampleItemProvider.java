package co.mobiwise.sample.slice;

import co.mobiwise.sample.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.List;

public class SampleItemProvider extends BaseItemProvider {
    private List<String> list;
    private Context slice;
    private ListenerCallBack listenerCallBack;

    public SampleItemProvider(List<String> list, Context slice) {
        this.list = list;
        this.slice = slice;
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int position) {
        if (list != null && position >= 0 && position < list.size()) {
            return list.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer componentContainer) {
        final Component cpt;
        if (convertComponent == null) {
            cpt = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_item_sample, null, false);
        } else {
            cpt = convertComponent;
        }
        Text text = (Text) cpt.findComponentById(ResourceTable.Id_text_setting);
        text.setText(list.get(position));
        cpt.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (listenerCallBack != null) {
                    listenerCallBack.onItemListener(component, position);
                }
            }
        });
        return cpt;
    }

    public void setItemListener(ListenerCallBack itemListener) {
        this.listenerCallBack = itemListener;
    }

    public interface ListenerCallBack {
        void onItemListener(Component component, int position);
    }
}
