/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package co.mobiwise.sample;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class LogUtil {
    private static final int DOMAIN = 0x001;

    public static void info(String tag, String format, Object... objects) {
        HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, DOMAIN, tag);
        HiLog.info(logLabel, format, objects);
    }

    public static void debug(String tag, String format, Object... objects) {
        HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, DOMAIN, tag);
        HiLog.debug(logLabel, format, objects);
    }

    public static void error(String tag, String format, Object... objects) {
        HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, DOMAIN, tag);
        HiLog.error(logLabel, format, objects);
    }

    public static void warn(String tag, String format, Object... objects) {
        HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, DOMAIN, tag);
        HiLog.warn(logLabel, format, objects);
    }

    public static void fatal(String tag, String format, Object... objects) {
        HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, DOMAIN, tag);
        HiLog.fatal(logLabel, format, objects);
    }
}