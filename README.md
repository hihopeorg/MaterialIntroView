# MaterialIntroView

**本项目是基于开源项目MaterialIntroView进行ohos化的移植和开发的，可以通过项目标签以及github地址
（ https://github.com/iammert/MaterialIntroView ）追踪到原项目版本**

#### 项目介绍

- 项目名称：MaterialIntroView
- 所属系列：ohos第三方组件适配移植
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/iammert/MaterialIntroView
- 原项目基线版本：v1.6.0 ,sha1: f7b36b77d691bd0f2b270b378fab8d1aaf8ea273
- 功能介绍：MaterialIntroView是展示性动画的库，辅助UI完成特定的动画效果
- 编程语言：Java
- 外部依赖：无

## 效果展示：

<img src="screenshot/效果展示.gif"/><img src="screenshot/效果展示1.gif"/>
<img src="screenshot/效果展示2.gif"/><img src="screenshot/效果展示3.gif"/>

#### 安装教程
方案一
1. 编译依赖的har包 MaterialIntroView.har。
2. 启动 DevEco Studio，将har包导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.har'])
	……
}
```
4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方案二
1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```
2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'co.mobiwise.ohos:materialintro:1.0.1'
}
```
#### 使用说明

1.创建动画实例，根据需求可自行扩展
```
    public static void performAnimation(Component component) {

        createAnimator animatorX = new createAnimator(component);
        createAnimator animatorY = new createAnimator(component);
        /**
         * 设置component 监听，更新UI，必须设置
         */
        component.setBindStateChangedListener(new Component.BindStateChangedListener() {
            @Override
            public void onComponentBoundToWindow(Component component) {
                animatorX.scaleX();
                animatorY.scaleY();
            }

            @Override
            public void onComponentUnboundFromWindow(Component component) {
                animatorX.stop();
                animatorY.stop();
            }
        });
    }

    private static class createAnimator {

        private final AnimatorProperty animatorProperty;
        private final float width;
        private final float height;

        public createAnimator(Component component) {
            /**
             * 获取当前component宽和高
             */
            width = component.getWidth();
            height = component.getHeight();

            /**
             * 创建属性动画实例
             */
            animatorProperty = new AnimatorProperty(component);
            /**
             * 设置透明度
             */
//            animatorProperty.alphaFrom(component.getPivotX()).alpha(0.9f);
            /**
             * 设置循环播放次数
             */
            animatorProperty.setLoopedCount(Animator.INFINITE);
            /**
             * 设置动画变化类型：匀速变化 Animator.CurveType.LINEAR 4
             */
            animatorProperty.setCurveType(Animator.CurveType.SMOOTH_STEP);
            /**
             * 设置持续时间
             */
            animatorProperty.setDuration(1000);
        }

        public void scaleX() {
            animatorProperty.scaleX(0.6f);
            animatorProperty.start();
        }

        public void scaleY() {
            animatorProperty.scaleY(0.6f);
            animatorProperty.start();
        }

        public void stop() {
            animatorProperty.stop();
        }
    }
```
2.设置组件透明度
```
    /**
     * 设置透明度
     */
    public static void createAlpha(Component component, int alpha) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setShape(OVAL);
        shapeElement.setAlpha(alpha);
        component.setBackground(shapeElement);
    }
```
3.设置圆角
```
    /**
     * 设置不同的圆角
     */
    public static Component createRadius(Component component, Context context) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setShape(ShapeElement.RECTANGLE);
        shapeElement.setCornerRadius(10f);
        shapeElement.setCornerRadiiArray(new float[]{20, 40, 30, 60, 20, 20, 100, 120});
        component.setBackground(shapeElement);
        return component;
    }
```
4.设置shape圆
```
    public static Component createCircular(Component component, Context context, RgbColor rgbColor, int width, int height) {
        component.setWidth(width);
        component.setHeight(height);
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setShape(OVAL);
        shapeElement.setRgbColor(rgbColor);
        component.setBackground(shapeElement);
        return component;
    }
```
版本迭代
    V1.0.1
#### 版权和许可信息

```
Copyright 2015 Mert Şimşek.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
