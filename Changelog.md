
#### version 1.0.1：

     1.优化标题栏
     2.调整RecyclerView Example 中 悬浮动画位置。
     3.删除DialogUtils。
     4.新增Toolbar Example 动画效果

#### version 1.0.0:
    
    1.目前支持功能如下

    - Demo Example
    - Gravity Example
    - Focus Example
    - RecyclerView Example
    - Tab Example

    2.因ohos api 暂时不支持的功能如下：

    - Toolbar Example 动画效果
    - settings 动画效果
    - search 动画效果
